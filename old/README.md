# Progetto di Elettronica - Overcurrent Protection

## Membri

- Franco Righetti
- Pietro Mazzini

## Link al progetto 

https://www.youtube.com/watch?v=7ctPSgaLxbc

## Immagine circuito

![](circuit.png)

![](https://cdn.instructables.com/ORIG/FWD/SW5D/J5MQQ58B/FWDSW5DJ5MQQ58B.jpg)

## Checklist componenti

* [ ] 1x Relay (12V one with 2 changeover contacts)
* [x] 2x LM358 OpAmp
* [ ] 1x 0.1Ω Resistor (we'll use 10Ω for now)
* [x] 6x 1kΩ Resistor
* [x] 1x 10kΩ Trimmer
* [x] 2x 20kΩ Resistor
* [ ] 1x Tactile Switch (NC)
* [X] 1x 1N4007 Diode
* [X] 2x BC547 NPN Transistor
* [X] 1x 5mm LED
* [X] 1x Breadboard
* [X] 1x Arduino
* [X] Nx Cables

## Comprensione del circuito

K2 FRT5 e' un rele' e sara' il componente principale del nostro circuito.
Se viene fatta passare corrente tra i pin 1 - 10, le coppie di pin 4 - 3 e
7 - 8 si chiudono. Noi andremo ad utilizzare i pin 7 - 8 per la corrente che
vogliamo controllare.

Aggiungiamo un transitor, nel nostro caso Q2 BC547, per controllare
la corrente tra pin 1 - 10, di conseguenza controllare i pin 4 - 3 e 7 - 8.

Il LED (D1) viene utilizzato per avere un feedback visivo sullo stato del rele'.
Quando il transistor Q2 BC547 e' chiuso, ovvero c'e' corrente tra i pin 1 - 10,
il led sara' acceso.

Per controllare la corrente in entrata, generiamo una differenza di potenziale
minima (questo per lavorare con una frazione della corrente entrante, quindi in
maniera piu' sicura) che poi andremo ad amplificare per avere un margine di
controllo piu' ampio (per poter paragornarla piu' semplicemente al
potenziometro). Se la DDP in 5 e' piu' grande di quella in 6, allora U2.2 LM358 
produrra' un segnale alto (basso in caso contrario).

Per controllare l'alimentazione tra i pin 1 - 10 usiamo una coppia di transistor.
Il primo (quello piu' in alto) server per controllare l'effettiva corrente tra i
pin, ovvero fornire un ground per creare una diffrenza di potenziale da VCC. Il 
secondo server per controllare il primo transistor. Il secondo transsitor viene 
controllato da due componenti: un bottone ed il comparatore. Il comparatore 
funzione come precedentemente esplicato, mentre il pulsante ha come funzione 
quella di impedire la creazione di un loop infinito.

Il loop consiste nel fatto che quando il comparatore manda il segnale per 
interrompere l'alimentazione, questi fa si che la VCC del carico principale 
cali (dato che il rele' si apre e si scollega) e che quindi il segnale del 
comparatore stesso torni basso, creando cosi' un loop infinito. Questo puo' 
essere gestito tramite l'utilizzo di un pulsante. Quest'ultimo fa da check 
manuale per fare in modo che si abbia il tempo di regolare il carico in modo 
tale che il comparatore non mandi un segnale di errore.

## Domande

- U1 1N4007?
    - Risposta del prof: "come ho detto a lezione il diodo serve per
      cortocircuitare le correnti inverse generate dall'avvolgimento del
      relais."

- Come fa la VCC a scegliere dove andare fra Q2 BC547 e l'altro ground?
    - Risposta del prof: "La VCC attaccata ad R6 serve per mantenere un livello
      logico alto alla base di Q1 quando Q2 è in interdizione.
      Quando Q2 non conduce la base di Q1 si trova a VCC (circa); quando Q2
      conduce la base di Q1 viene posta (quasi) a GND."

- Alimentazione Op Amp; serve a qualcosa di particolare o serve e basta? 
  Direzione dell'alimentazione?

- Perchè la R2 va verso terra? Non sarebbe uguale se non ci fosse?
